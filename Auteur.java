public class Auteur {
    private String nom;
    private int qualiteTragedie;
    private String citation1;
    private int qualiteComedie;
    private String citation2;
    private int qualiteDrame;
    private String citation3;
        
    public Auteur(String nom, int qualiteTragedie, String citation1, int qualiteComedie,String citation2, int qualiteDrame,String citation3){
        this.nom = nom;
        this.qualiteTragedie = qualiteTragedie;
        this.citation1 = citation1;
        this.qualiteComedie = qualiteComedie;
        this.citation2 = citation2;
        this.qualiteDrame = qualiteDrame;
        this.citation3 = citation3;
    }

    public int getQualiteTragedie(){
        return this.qualiteTragedie;
    }
    public String getCitationTragedie(){
        return this.citation1;
    }

    public String getCitationComedie(){
        return this.citation2;
    }
    public int getQualiteComedie(){
        return this.qualiteComedie;
    }

    public String getCitationDrame(){
        return this.citation3;
    }
    public int getQualiteDrame(){
        return this.qualiteDrame;
    }

    public Style pointFort(){
        Style meilleur = null;
        int tragedie = getQualiteTragedie();
        int comedie = getQualiteComedie();
        int drame = getQualiteDrame();
        if (tragedie > comedie && tragedie > drame){
            meilleur = Style.TRAGÉDIE;
        }
        if (comedie > tragedie && comedie > drame){
            meilleur = Style.COMÉDIE;
        }
        if (drame > comedie && drame > tragedie){
            meilleur = Style.DRAME;
        }
        return meilleur;
    }

    public int qualiteStyle(Style s){     
        int qualite = 0;
        if (s == Style.TRAGÉDIE){
            qualite = getQualiteTragedie();
        }
        if ( s == Style.COMÉDIE){
            qualite = getQualiteComedie();
        }
        if (s == Style.DRAME){
            qualite = getQualiteDrame();
        }
        return qualite;
    }

    public String citationStyle(Style s){
        String citation = "";
        if (s == Style.TRAGÉDIE){
            citation = getCitationTragedie();
        }
        if ( s == Style.COMÉDIE){
            citation = getCitationComedie();
        }
        if (s == Style.DRAME){
            citation = getCitationDrame();
        }
        return citation; 
    }

    public String toString(){
        return "L'honorable"+this.nom;
    }
}
